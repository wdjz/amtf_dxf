﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace amtf_dxf
{
    public partial class Form1 : Form
    {
        string 程序路径 = System.IO.Directory.GetCurrentDirectory();

        public Form1()
        {
            InitializeComponent();
            //this.button1.Visible = false;
            Control.CheckForIllegalCrossThreadCalls = false; //加载时 取消跨线程检查
            // 靠右下角↓
            //this.Location = new Point(Screen.PrimaryScreen.WorkingArea.Width - this.Size.Width,
            //Screen.PrimaryScreen.WorkingArea.Height - this.Size.Height);
            // 靠右下角↓
            this.Location = new Point(SystemInformation.WorkingArea.Width - this.Width - 18,
            SystemInformation.WorkingArea.Height - this.Height - 18);
            //this.BackgroundImage = Image.FromFile("./amtf-透明.png");
            调整文本框大小();


            // Create the ToolTip and associate with the Form container.
            ToolTip toolTip1 = new ToolTip();
            // Set up the delays for the ToolTip.
            toolTip1.AutoPopDelay = 5000;
            toolTip1.InitialDelay = 500;
            toolTip1.ReshowDelay = 500;
            // Force the ToolTip text to be displayed whether or not the form is active.
            toolTip1.ShowAlways = true;
            // Set up the ToolTip text for the Button and Checkbox.
            toolTip1.SetToolTip(this.label1, this.label1.Text);
        }

        private void textBox1_TextChanged(object sender, EventArgs e)
        {

        }

        private void Form1_SizeChanged(object sender, EventArgs e)
        {
            调整文本框大小();
        }
        private void 调整文本框大小()
        {
            this.richTextBox1.Height = (int)((double)this.Height * 0.5);
            this.richTextBox1.Width = (int)((double)this.Width * 0.6);
        }

        private void richTextBox1_TextChanged(object sender, EventArgs e)
        {
            //richTextBox1.SelectionStart = richTextBox1.Text.Length; //Set the current caret position at the end
            //richTextBox1.ScrollToCaret(); //Now scroll it automatically
        }

        private void button1_Click(object sender, EventArgs e)
        {
            //string message = @"调用amtf_dxf|D:/amtf/00南腾家居-项目/0915鸟笼/导出20221017_202040/布局盒子#17-__1_6_00_1_-1____板厚9.dxf|1|true_12_1 ";
            //string[] ss = message.Split('|');
            //string dxf全名 = ss[1];
            //string 计数 = ss[2];
            //string 封边信息 = ss[3];
            //amtf_dxf.处理dxf图层(dxf全名, 计数, 封边信息);

            //amtf_dxf.三dl转2dl测试();
            amtf_dxf.ToPolyline2D();
;
        }
    }
}
